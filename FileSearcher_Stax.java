/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

/**
 *
 * @author Near
 */
@WebServlet(name = "FileSearcher_Stax", urlPatterns = {"/FileSearcher_Stax"})
public class FileSearcher_Stax extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            File fileKey = new File("D:/Workspace/Netbeans/xml_lab4/FileSearcher_Stax/keyword.txt");
        FileInputStream fis = new FileInputStream(fileKey);
        InputStreamReader isr = new InputStreamReader(fis, "UTF-8");
        BufferedReader br = new BufferedReader(isr);
        ArrayList<String> keywords = new ArrayList<String>();
        keywords.clear();
        while (br.ready()) {
            keywords.add(br.readLine());
        }
        br.close();

        // Boolean store help find quotes
        boolean authorFound = false;
        boolean wordsFound = false;

        // String store data
        String author = null;
        String words = null;

        XMLInputFactory factory = XMLInputFactory.newInstance();
        XMLStreamReader parser = null;

        if (!keywords.isEmpty()) {
            for (String keyword : keywords) {
                try {
                    //Input file
                    File fileInput = new File("D:/Workspace/Netbeans/xml_lab4/FileSearcher_Stax/quotes.xml");
                    FileInputStream fis2 = new FileInputStream(fileInput);
                    parser = factory.createXMLStreamReader(fis2);
                
                while (parser.hasNext()) {
                    
                    int tmp = parser.next();
                    if (parser.isStartElement()) {
                        String eName = parser.getLocalName();
                        if (eName.equals("by")) {
                            authorFound = true;
                        } else if (eName.equals("words")) {
                            wordsFound = true;
                        }
                    } else if (parser.isEndElement()) {
                        if (authorFound == true) {
                            if (author.toLowerCase().contains(keyword.toLowerCase())) {
                                out.print(words + " <br/>by "
                                        + author + "<br/><br/>");
                            }
                            authorFound = false;
                        } else if (wordsFound == true) {
                            wordsFound = false;
                        }
                    } else if (parser.isCharacters()) {
                        if (authorFound == true) {
                            author = parser.getText();
                        } else if (wordsFound == true) {
                            words = parser.getText();
                        }
                    }
                }
                    parser.close();
                } catch (XMLStreamException ex) {
                    out.print(ex.toString());
                }
            }
        }
    
            
            
        } catch(Exception ex) {            
            out.print(ex.toString());
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
