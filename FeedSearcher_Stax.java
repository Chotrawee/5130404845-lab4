/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.URL;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.stream.*;
import javax.xml.stream.events.*;

/**
 *
 * @author Near
 */
@WebServlet(name = "FeedSearcher_Stax", urlPatterns = {"/FeedSearcher_Stax"})
public class FeedSearcher_Stax extends HttpServlet {
    
    XMLEventReader reader;
    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws IOException {
        
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        PrintWriter out = response.getWriter();
        String uLoc = request.getParameter("url");
        String kw = request.getParameter("kw");
        boolean titleFound = false;
        boolean linkFound = false;
        boolean itemFound = false;
        
        //Store found title match keyword
        boolean foundTitle = false;
        
        String eName;
        try {
            URL url = new URL(uLoc);
            InputStream iStream = url.openStream();
            XMLInputFactory xFactory = XMLInputFactory.newInstance();
            reader = xFactory.createXMLEventReader(iStream);
            
            out.print("<html><body><table border = '1'><tr><th>Title</th><th>Link</th></tr>");
            while (reader.hasNext()) {
                XMLEvent event = reader.nextEvent();
                
                if (event.isStartElement()) { 
                    StartElement element = (StartElement) event;
                    eName = element.getName().getLocalPart();
                    if (eName.equals("item")) {
                        itemFound = true;
                    }
                    if (itemFound && eName.equals("link")) {
                        linkFound = true;
                    }
                    if (itemFound && eName.equals("title")) {
                        titleFound = true;
                    }
                }
                
                else if (event.isEndElement()) {
                    EndElement element = (EndElement) event;
                    eName = element.getName().getLocalPart();
                    if (eName.equals("item")) {
                        itemFound = false;
                    }
                    if (itemFound && eName.equals("link")) {
                        out.print("</td></tr>");
                        linkFound = false;
                        foundTitle = false;
                    }
                    if (itemFound && eName.equals("title")) {
                        out.print("</td>");
                        titleFound = false;
                    }
                }
                
                else if (event.isCharacters()) {
                    Characters characters = (Characters) event;
                    if (linkFound) {
                        if(foundTitle) {
                            String link = characters.getData();
                            out.print("<td><a href='" + link + "'>" + link + "</a>");
                        }
                    }
                    if (titleFound) {
                        String title = characters.getData();
                        String keyword = kw.toLowerCase();
                        if(title.toLowerCase().contains(keyword)) {
                            foundTitle = true;
                            out.print("<tr><td>");
                            out.print(title);
                        }
                    }
                }
            }			//	end	while
            reader.close();
            out.print("</table></body></html>");
        } catch (Exception ex) {
            ex.printStackTrace(System.err);
        } finally {
            out.close();
        }    
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
