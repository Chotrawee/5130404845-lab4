/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.*;
import org.w3c.dom.*;

/**
 *
 * @author Near
 */
@WebServlet(name = "FileSearcher", urlPatterns = {"/FileSearcher"})
public class FileSearcher extends HttpServlet {
    FileSearcher fs;
    String filename;
    List<String> keywords = new ArrayList<String>();
    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        
        
            PrintWriter out = response.getWriter();
        try {
            setFilename("D:/Workspace/Netbeans/xml_lab4/FileSearcher/quotes.xml");
            File inputFile = new File(getFilename());
            setFilename("D:/Workspace/Netbeans/xml_lab4/FileSearcher/keyword.txt");
            File keywordFile = new File(getFilename());
            
            //Create new instance of DOM to parse the file
            request.setCharacterEncoding("UTF-8");
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document doc = builder.parse(inputFile);
            
            FileInputStream fis = new FileInputStream(keywordFile);
            InputStreamReader isr = new InputStreamReader(fis, "UTF-8");
            BufferedReader reader = new BufferedReader(isr);
            readFile(reader);
            
            NodeList items = doc.getElementsByTagName("quote");

            if(keywords.size() > 1) {
                for(int i = 0; i < items.getLength(); i++) {
                    Element item = (Element)items.item(i);
                    String author = getElemVal(item,"by");
                    
                    for(int j = 0; j < keywords.size() - 1; j++) {
                        String keyword = keywords.get(j).toLowerCase();
                        
                        if(author.toLowerCase().contains(keyword)) {
                            out.println(getElemVal(item, "words") + "<br/>by "
                                    + author + "<br/><br/>");
                        }
                    }
                }
            }
            
            else {
                for(int i = 0; i < items.getLength(); i++) {
                    Element item = (Element)items.item(i);
                    String author = getElemVal(item,"by");
                    out.println(getElemVal(item, "words") + "<br/>by "
                                    + author + "<br/><br/>");
                }
            }
        }
        catch (FileNotFoundException ex) {
            out.print("File \"Keyword.txt\" does not exist at " + getFilename() + "<br/>");
        }
        catch(Exception ex){            
            out.print(ex.toString() + "<br/>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    protected String getFilename() {
        return filename;
    }
    
    protected void setFilename(String name) {
        filename = name;
    }

    private void readFile(BufferedReader reader) {
        try {
            int i = 0;
            while(true) {
                keywords.add(i, reader.readLine());
                if(keywords.get(i).equals("")) {
                break;
                }
                i++;
            }
            
        }
        catch(Exception ex) {
            System.out.print(ex.toString());
        }
    }
    
    protected String getElemVal(Element parent, String lable) {
        Element e = (Element) parent.getElementsByTagName(lable).item(0);
        try {
            Node child = e.getFirstChild();
            if(child instanceof CharacterData) {
                CharacterData cd = (CharacterData) child;
                return cd.getData();
            }
            return "";
        }
        catch(Exception ex) {
            return ex.toString();
        }
    }
}


