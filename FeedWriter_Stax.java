/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.*;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.stream.*;
import javax.xml.stream.events.*;
import javax.xml.transform.*;
import javax.xml.transform.stream.*;

/**
 *
 * @author Near
 */
@WebServlet(name = "FeedWriter_Stax", urlPatterns = {"/FeedWriter_Stax"})
public class FeedWriter_Stax extends HttpServlet {
    XMLEventFactory eventFactory = XMLEventFactory.newInstance();
    
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        
        String title = request.getParameter("title");
        String link = request.getParameter("link");
        String desc = request.getParameter("desc");
        
        try {
            out.print(createRssDoc(title, link, desc));
        } catch (Exception e) {
            System.out.print(e);
        }
        out.close();
    }

    public String createRssDoc(String title, String link, String desc) throws Exception {

        String filename = "D:/Workspace/Netbeans/xml_lab4/FeedWriter_Stax/feed.xml";
        File file = new File(filename);

        String tmpXml;
        
        // Store output
        StringWriter sw = new StringWriter();

        if (file.exists()) {
            //Reader
            FileInputStream fis = new FileInputStream(file);
            InputStreamReader isr = new InputStreamReader(fis, "UTF-8");
            XMLInputFactory input = XMLInputFactory.newInstance();
            XMLEventReader reader = input.createXMLEventReader(isr);
            
            //Writer
            XMLOutputFactory output = XMLOutputFactory.newInstance();
            XMLEventWriter writer = output.createXMLEventWriter(sw);

            while (reader.hasNext()) {
                XMLEvent event = reader.nextEvent();
                if (event.isEndElement()) {
                    EndElement element = (EndElement) event;
                    String eName = element.getName().getLocalPart();
                    if (eName.equals("channel")) {
                        writer.add(eventFactory.createStartElement("", "", "item"));
                        writer.add(eventFactory.createStartElement("", "", "title"));
                        writer.add(eventFactory.createCharacters(title));
                        writer.add(eventFactory.createEndElement("", "", "title"));
                        writer.add(eventFactory.createStartElement("", "", "description"));
                        writer.add(eventFactory.createCharacters(desc));
                        writer.add(eventFactory.createEndElement("", "", "description"));
                        writer.add(eventFactory.createStartElement("", "", "link"));
                        writer.add(eventFactory.createCharacters(link));
                        writer.add(eventFactory.createEndElement("", "", "link"));
                        writer.add(eventFactory.createStartElement("", "", "pubDate"));
                        writer.add(eventFactory.createCharacters((new java.util.Date()).toString()));
                        writer.add(eventFactory.createEndElement("", "", "pubDate"));
                        writer.add(eventFactory.createEndElement("", "", "item"));
                    } else {
                        writer.add(event);
                    }
                } else {
                    writer.add(event);
                }
            }
            
            writer.flush();
            writer.close();
            reader.close();
            
        } else {
            XMLOutputFactory xof = XMLOutputFactory.newFactory();
            XMLStreamWriter xtw = xof.createXMLStreamWriter(sw);

            xtw.writeStartDocument();
            xtw.writeStartElement("rss");
            xtw.writeAttribute("version", "2.0");
            xtw.writeStartElement("channel");
            xtw.writeStartElement("title");
            xtw.writeCharacters("Feed Writer Stax");
            xtw.writeEndElement();//end title
            xtw.writeStartElement("description");
            xtw.writeCharacters("Feed Writer Stax");
            xtw.writeEndElement();//end description
            xtw.writeStartElement("link");
            xtw.writeCharacters("http://www.kku.ac.th");
            xtw.writeEndElement();//end link
            xtw.writeStartElement("lang");
            xtw.writeCharacters("en~th");
            xtw.writeEndElement();//end lang
            //element/rss/channel/item
            xtw.writeStartElement("item");
            //element/rss/channel/item/title
            xtw.writeStartElement("title");
            xtw.writeCharacters(title);
            xtw.writeEndElement();
            xtw.writeStartElement("description");
            xtw.writeCharacters(desc);
            xtw.writeEndElement();
            xtw.writeStartElement("link");
            xtw.writeCharacters(link);
            xtw.writeEndElement();
            xtw.writeStartElement("pubDate");
            xtw.writeCharacters((new java.util.Date()).toString());
            xtw.writeEndElement();
            xtw.writeEndElement();//end	element	item
            xtw.writeEndElement();//end	element	channel
            xtw.writeEndElement();//end	element	rss
            xtw.writeEndDocument();
            xtw.flush();
            xtw.close();
        }
        //Transform xml to xml document
        TransformerFactory factory = TransformerFactory.newInstance();

        Transformer transformer = factory.newTransformer();
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");

        StringWriter formattedStringWriter = new StringWriter();
        StringReader strReader = new StringReader(sw.toString());
        StreamResult stmResult = new StreamResult(formattedStringWriter);
        StreamSource stmSource = new StreamSource(strReader);
        transformer.transform(stmSource, stmResult);
        tmpXml = formattedStringWriter.toString();

        
        FileOutputStream fos = new FileOutputStream(file, false);
        OutputStreamWriter osw = new OutputStreamWriter(fos, "UTF-8");
        BufferedWriter bw = new BufferedWriter(osw);
        bw.write(tmpXml);
        bw.flush();
        bw.close();
        return tmpXml;    
    }
    
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
