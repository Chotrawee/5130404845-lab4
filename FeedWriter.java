/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.*;
import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import org.w3c.dom.*;
import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;

@WebServlet(name = "FeedWriter", urlPatterns = {"FeedWriter"})
public class FeedWriter extends HttpServlet {
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        String filename = "D:/Workspace/Netbeans/xml_lab4/FeedWriter/feed.xml";
    try {
        File file = new File(filename);
        if(!(file.exists())) {    
            try {
                DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
                DocumentBuilder docBuilder = builderFactory.newDocumentBuilder();
                //creating a new instance of a DoM to build	a DOM tree.	 
                Document doc = docBuilder.newDocument();
                FeedWriter fw = new FeedWriter();
                String feed = fw.createRssTree(doc);
                
                out.print(feed);           

            } catch (Exception e) {
                System.out.println(e);
            }
        }
        else {
            try {
                DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
                factory.setNamespaceAware(true);
                DocumentBuilder parser = factory.newDocumentBuilder();	 
                Document doc = parser.parse(file);
                String feed;
                
                request.setCharacterEncoding("UTF-8");
                String title = request.getParameter("title");
                String link = request.getParameter("link");
                String desc = request.getParameter("desc");
                
                if(title.equals("")){
                    feed = fileReader(doc);
                }
            
                else  {
                    FeedWriter fw = new FeedWriter();
                    feed = fw.appendTree(doc, title, link, desc);
                }
            
            out.print(feed);
        }
        
        catch(NullPointerException e) {
       
            DocumentBuilderFactory dfactory = DocumentBuilderFactory.newInstance();
            dfactory.setNamespaceAware(true);
            DocumentBuilder parser = dfactory.newDocumentBuilder();	 
            Document doc = parser.parse(file);
            
            String feed = fileReader(doc);
            out.print(feed);
        }
        
        catch(Exception e) {
            out.print(e.toString());
        }
    }
    }
    catch(Exception e){
        out.print(e.toString());
    }
}
    

    public String createRssTree(Document doc) throws Exception {
                
        Element rss = doc.createElement("rss");

        //attribute version= '2.0'in element rss	 
        rss.setAttribute("version", "2.0");
        doc.appendChild(rss);

        //the child element of rss is channel	 
        Element channel = doc.createElement("channel");
        rss.appendChild(channel);
        
        
        //Element/rss/channel/title	 
        Element title = doc.createElement("title");
        channel.appendChild(title);
        Text titleT = doc.createTextNode("Khon Kean University Rss Feed");
        title.appendChild(titleT);

        //element/rss/channel/description	 
        Element desc = doc.createElement("description");
        channel.appendChild(desc);
        Text descT = doc.createTextNode("Khon Kean University Information News Rss Feed");
        desc.appendChild(descT);
        
        //element/rss/channel/link
        Element link = doc.createElement("link");
        channel.appendChild(link);
        Text linkT = doc.createTextNode("http://www.kku.ac.th");
        link.appendChild((linkT));
        
        //element/rss/channel/lang
        Element lang = doc.createElement("lang");
        channel.appendChild(lang);
        Text langT = doc.createTextNode("en-th");
        lang.appendChild(langT);
        
        
        TransformerFactory factory = TransformerFactory.newInstance();
        Transformer transformer = factory.newTransformer();

        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        
        StringWriter sw = new StringWriter();
        StreamResult result = new StreamResult(sw);
        DOMSource source = new DOMSource(doc);
        transformer.transform(source, result);
        String xmlString = sw.toString();

        //set to an appropriate	file name in the web project
        String filename = "D:/Workspace/Netbeans/xml_lab4/FeedWriter/feed.xml";
        File file = new File(filename);
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file)));
        bw.write(xmlString);
        bw.flush();
        bw.close();
        return xmlString;
    }
    
    public String appendTree(Document doc, String myTitle, String myLink, String myDesc) throws Exception {
        
        NodeList channelNode = doc.getElementsByTagName("channel");
        Element channel = (Element)channelNode.item(0);
        
        //element/rss/channel/item	 
        Element item = doc.createElement("item");
        channel.appendChild(item);

        //element/rss/channel/title	 
        Element iTitle = doc.createElement("title");
        item.appendChild(iTitle);
        Text iTitleT = doc.createTextNode(myTitle);
        iTitle.appendChild(iTitleT);
        
        //element/rss/channel/description
        Element iDesc = doc.createElement("description");
        item.appendChild(iDesc);
        Text iDescT = doc.createTextNode(myDesc);
        iDesc.appendChild(iDescT);

        //element/rss/channe/link
        Element iLink = doc.createElement("link");
        item.appendChild(iLink);
        Text iLinkT = doc.createTextNode(myLink);	 
        iLink.appendChild(iLinkT);
        
        //element/rss/channe/pubDate
        Element pubDate = doc.createElement("pubDate");
        item.appendChild(pubDate);
        Text pubDateT = doc.createTextNode((new java.util.Date()).toString());
        pubDate.appendChild(pubDateT);
        
        //Transformer building Rss        
        TransformerFactory factory = TransformerFactory.newInstance();
        Transformer transformer = factory.newTransformer();

        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        
        StringWriter sw = new StringWriter();
        StreamResult result = new StreamResult(sw);
        DOMSource source = new DOMSource(doc);
        transformer.transform(source, result);
        String xmlString = sw.toString();

        //set to an appropriate	file name in the web project
        String filename = "D:/Workspace/Netbeans/xml_lab4/FeedWriter/feed.xml";
        File file = new File(filename);
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file),"UTF-8"));
        bw.write(xmlString);
        bw.flush();
        bw.close();
        return xmlString;
    }
    
    private String fileReader(Document doc) throws ServletException , IOException{
        try {
            TransformerFactory factory = TransformerFactory.newInstance();
            Transformer transformer = factory.newTransformer();

            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        
            StringWriter sw = new StringWriter();
            StreamResult result = new StreamResult(sw);
            DOMSource source = new DOMSource(doc);
            transformer.transform(source, result);
            String xmlString = sw.toString();
            return xmlString;
        }
        catch(Exception e) {
            return e.toString();
        }
    }
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
    
    @Override
    public String getServletInfo() {
        return "Short description";
    } // </editor-fold>
    
}
    