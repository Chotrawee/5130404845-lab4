/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.*;
import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import org.w3c.dom.*;
import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;
/**
 *
 * @author Near
 */
@WebServlet(name = "FileWriter", urlPatterns = {"/FileWriter"})
public class FileWriter extends HttpServlet {
    String filename;
    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        try {
                // Create new instance of a DOM to build a dom tree
                DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
                DocumentBuilder docBuilder = dbFactory.newDocumentBuilder();
                Document doc = docBuilder.newDocument();
                
                FileWriter fw = new FileWriter();
                String write = fw.createTree(doc);
                out.print(write);
        }
        catch(Exception ex) {
            out.print(ex.toString());
        }
        
    }
    
    protected String createTree(Document doc) {
        // Create Element quote1
        Element quote = doc.createElement("quote");
        quote.appendChild(doc.createTextNode("\n\t\t"));
        
        Element words = doc.createElement("words");
        quote.appendChild(words);
        
        words.appendChild(doc.createTextNode("Time is more value than money. You can get more money, but you cannot get more time."));
        
        quote.appendChild(doc.createTextNode("\n\t\t"));
        Element by = doc.createElement("by");
        quote.appendChild(by);
        
        by.appendChild(doc.createTextNode("Jim Rohn"));
        
        quote.appendChild(doc.createTextNode("\n\t"));
        
        // Create Element quote2
        Element quote2 = doc.createElement("quote");
        
        quote2.appendChild(doc.createTextNode("\n\t\t"));
        Element words2 = doc.createElement("words");
        quote2.appendChild(words2);
        
        words2.appendChild(doc.createTextNode("เมื่อทำอะไรสำเร็จ แม้เป็นก้าวเล็กๆ ของตัวเอง ก็ควรรู้จักให้รางวัลตัวเองบ้าง"));
        
        quote2.appendChild(doc.createTextNode("\n\t\t"));
        Element by2 = doc.createElement("by");
        quote2.appendChild(by2);
        
        by2.appendChild(doc.createTextNode("ว. วชิรเมธี"));
        quote2.appendChild(doc.createTextNode("\n\t"));
        
        // Add Element quote and quote2 to Element quotes
        // Add quotes Element to be root node
        Element quotes = doc.createElement("quotes");
        quotes.appendChild(doc.createTextNode("\n\t"));
        quotes.appendChild(quote);
        quotes.appendChild(doc.createTextNode("\n\t"));
        quotes.appendChild(quote2);
        doc.appendChild(quotes);
        
        try {
        TransformerFactory factory = TransformerFactory.newInstance();
        Transformer transformer = factory.newTransformer();
        
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        
        StringWriter sw = new StringWriter();
        StreamResult result = new StreamResult(sw);
        DOMSource source = new DOMSource(doc);
        transformer.transform(source, result);
        String xmlString = sw.toString();

        //set to an appropriate	file name in the web project
        setFilename("D:/Workspace/Netbeans/xml_lab4/FileWriter/quotes.xml");
        File file = new File(filename);
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file),"UTF-8"));
        bw.write(xmlString);
        bw.flush();
        bw.close();
        
        return xmlString;
        }
        catch(Exception ex) {
            return ex.toString();
        }
        
    }
    
    protected void setFilename(String name) {
        filename = name;
    }
    
    protected String getFilename() {
        return filename;
    }
    
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}